#include "brain.h"
/*
sets up codes memory and runs on the file for next command
in: filname
*/
void read_code(const char * filename)
{
	FILE* file = open_file(filename);
	char memory[1000] = { 0 };
	char* ptr = memory;
	char command = NULL;
	do {
		command = fgetc(file);
		parse(command, &ptr, file);

	} while (command != EOF);
	fclose(file);
}

FILE * open_file(const char * filename)
{
	FILE* fp;
	fopen_s(&fp, filename, "r");
	if (!fp) {
		printf("Error opening file\n");
		exit(1);
	}
	return fp;
}

/*
parses and enacts each command
in: command, pointer to memory pointer, file stream
*/
void parse(char command, char** ptr, FILE* stream)
{
	static long* open_bracket = NULL; // stores all open bracket locations in file (for loops)
	static size_t len = 0;
	void* temp= NULL;
	switch (command) {
	case '<': // move pointer one back
		--(*ptr);
		break;
	case '>': // move pointer 1 foward
		++(*ptr);
		break;
	case '+': //increment data IN pointer
		++(**ptr);
		break;
	case '-': // decrement data IN pointer
		--(**ptr);
		break;
	case '.': // print data in pointer
		putchar(**ptr);
		break;
	case '[': //open loop
		++len;
		temp = (long*)realloc(open_bracket, sizeof(long)*len);
		if (!temp) {
			free(open_bracket);
			exit(1);
		}
		open_bracket = (long*)temp;
		open_bracket[len - 1] = ftell(stream); // save open loop location
		break;
	case ']': //close loop
		if (**ptr) { //if while data is nonzero (while true) jump back to open loop
			fseek(stream, open_bracket[len - 1], SEEK_SET);// jump to most recent open loop
		}
		else {// remove open loop location from memory
			--len;
			if (!len) {
				free(open_bracket);
				open_bracket = NULL;
			}
			else {
				temp = (long*)realloc(open_bracket, sizeof(long)*len);
				if (!temp) {
					free(open_bracket);
					exit(1);
				}
				open_bracket = (long*)temp;
			}
			
			break;
	case EOF:
		break;
	default:
		printf("Invalid Command");
		exit(0);

		}
	}
}
