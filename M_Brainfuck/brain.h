#pragma once
#include <stdio.h>


void read_code(const char* filename);
FILE* open_file(const char* filename);
void parse(char command, char** ptr, FILE* stream);
